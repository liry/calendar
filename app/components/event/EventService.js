(function () {
    'use strict';
    angular
        .module('event.service', [])
        .service('EventService', ['$http', '$filter', '$rootScope', '$q', '$cacheFactory', EventService]);

    function EventService($http, $filter, $rootScope, $q, $cacheFactory) {

        var Service = {
            cache: $cacheFactory('events'),
            events: events,
            getById: getById,
            save: save
        };

        function save(event) {
            console.log(event);
            if (event.id > 0) {
                angular.extend($filter('filter')(Service.cache.get('all'), {id: event.id})[0], event);
                $rootScope.$broadcast("UPDATE_EVENT", event);
            } else {
                var events = Service.cache.get('all'),
                    maxId = 0;
                for (var i in events) {
                    if (events[i].id > maxId) maxId = events[i].id
                }
                event.id = maxId + 1;
                events.push(event);
                Service.cache.put('all', events);
                $rootScope.$broadcast("NEW_EVENT", event);
            }
        }

        function events(calendar) {
            calendar = calendar || "all";
            var defer = $q.defer();
            function getFromCache() {
                if (calendar == "all") return angular.copy(Service.cache.get('all'));
                else {
                    return $filter('filter')(Service.cache.get('all'), {calendar: calendar});
                }
            }

            if (Service.cache.get('all') == undefined) {
                $http.get('data/events.json', {params: {calendar: calendar}}).success(function (response) {
                    Service.cache.put('all', response.events);
                    defer.resolve(getFromCache());
                });
            } else {
                defer.resolve(getFromCache());
            }

            return defer.promise;
        }

        function getById(id) {
            var event = {};
            Service.events().then(function (events) {
                var e = {};
                for (var i in events) {
                    if (events[i].id == id) {
                        e = events[i];
                        break;
                    }
                }
                angular.extend(event, e);
            });

            return event;
        }

        $rootScope.$on("DELETE_EVENT", function (e, event) {
            Service.cache.put('all',
                $filter('filter')(Service.cache.get('all'), function (item) { return item.id != event.id }));
        });

        return Service;
    }

})();