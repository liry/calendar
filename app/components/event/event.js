(function () {
    'use strict';
    angular
        .module('event', [
            'ui.router',
            'event.ctrl',
            'xeditable'])
        .config(['$stateProvider', UrlMapping])
        .run(function(editableOptions) {
            editableOptions.theme = 'bs3';
        });

    function UrlMapping($stateProvider) {
        $stateProvider.state('desk.calendar.newEvent', {
            url: "/new",
            params: {
                "event": {
                    "title": "Draft Event",
                    "start": new Date().toJSON(),
                    "end": new Date().toJSON(),
                    "description": "N/A"
                }
            },
            templateUrl: 'components/event/event.html',
            controller: 'EventCtrl',
            controllerAs: 'event'
        }).state('desk.calendar.event', {
            url: "/:id",
            params: {
                "event": null
            },
            templateUrl: 'components/event/event.html',
            controller: 'EventCtrl',
            controllerAs: 'event'
        });
    }
})();
