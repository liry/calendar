(function () {
    angular.module('event.directive', [])
        .directive('event', ['$state', '$rootScope', EventDirective]);

    function EventDirective($state, $rootScope) {
        var directive = {
            restrict: 'E',
            templateUrl: 'components/event/eventDirective.html',
            controller: function ($scope, $element) {
                $scope.openEvent = function () {
                    if (/(event|newEvent)$/.test($state.current.name)) {
                        $state.go('^.event', {id: $scope.event.id});
                    } else {
                        $state.go('.event', {id: $scope.event.id});
                    }
                };
                $scope.deleteEvent = function () {
                    $rootScope.$broadcast("DELETE_EVENT", $scope.event);
                };

                $scope.$on('UPDATE_EVENT', function (e, event) {
                    if (event.id == $scope.event.id) angular.extend($scope.event, event);
                });
            }
        };

        return directive;
    }
})();

