(function () {
    angular.module('event.ctrl', ['ui.router'])
        .controller('EventCtrl', ['$state', '$scope', 'EventService', EventCtrl]);

    function EventCtrl($state, $scope, events) {
        var self = this,
            event = $state.params.event;
        self.selected = event ? angular.extend({calendar: $state.params.calendar}, event) : events.getById($state.params.id);
        self.update = function () { events.save(self.selected); };

        $scope.$on('NEW_EVENT', function(e, createdEvent) {
           if (self.selected.id == createdEvent.id) $state.go("^.event", {id: createdEvent.id});
        });

        $scope.$on('UPDATE_EVENT', function (e, updatedEvent) {
            if (self.selected.id == updatedEvent.id) angular.extend(self.selected, updatedEvent);
        });

        $scope.$on('DELETE_EVENT', function (e, deletedEvent) {
            if (self.selected.id == deletedEvent.id) $state.go('^');
        });
    }
})();
