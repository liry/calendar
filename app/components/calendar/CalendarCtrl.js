(function () {
    'use strict';
    angular.module('calendar.ctrl', ['event.service'])
        .controller('CalendarCtrl', ['EventService', '$state', '$scope', '$filter', CalendarCtrl]);

    function CalendarCtrl(EventService, $state, $scope, $filter) {
        var self = this;
        self.current = $state.params.calendar;
        self.events = [];
        EventService.events(self.current).then(function (events) {
            self.events = events;
        });

        $scope.$on("NEW_EVENT", function (e, event) {
            if (event.calendar == self.current) {
                self.events.push(event);
            }
        })

        $scope.$on("DELETE_EVENT", function (e, event) {
            if (event.calendar == self.current || self.current == "all") {
                self.events = $filter('filter')(self.events, function (item) { return item.id != event.id });
            }
        })
    }
})();
