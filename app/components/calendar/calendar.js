(function () {
    'use strict';
    angular
        .module('calendar', [
            'ui.router',
            'calendar.ctrl',
            'calendar.filter',
            'event',
            'event.directive'])
        .config(['$stateProvider', UrlMapping]);

    function UrlMapping($stateProvider) {
        $stateProvider.state('desk.calendar', {
            url: "/:calendar",
            templateUrl: "components/calendar/calendar.html",
            controller: "CalendarCtrl",
            controllerAs: "calendar"
        });
    }
})();
