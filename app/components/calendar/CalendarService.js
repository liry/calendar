(function () {
    'use strict';
    angular.module('calendar.service', [])
        .service('CalendarService', CalendarService);
    function CalendarService($http) {
        var Service = {
            list: new Calendars().calendars
        };

        function Calendars() {
            var self = this;
            self.calendars = {"all": {"sort": 0, "title": "All Events"}};
            self.$promise = $http.get('data/calendars.json').success(function (response) {
                angular.extend(self.calendars, response.calendars);
                return self.calendars;
            });
        }

        return Service;
    };
})();

