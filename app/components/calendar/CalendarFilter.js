(function() {
    angular.module('calendar.filter', [])
        .filter('calendar', Filter);

    function Filter() {
        return function(events, calendar) {
            if (calendar == 'all') return events;
            else {
                var filtered = [];
                for (var i in events) {
                    if (events[i].calendar == calendar) filtered.push(events[i]);
                }
                return filtered;
            }
        }
    }
})();