(function () {
    'use strict';
    angular
        .module('desk', [
            'ui.router',
            'desk.ctrl',
            'calendar'])
        .config(['$stateProvider', UrlMapping]);

    function UrlMapping($stateProvider) {
        $stateProvider.state('desk', {
            url: "",
            templateUrl: "components/desk/desk.html",
            controller: "DeskCtrl",
            controllerAs: "desk"
        });
    }
})();
