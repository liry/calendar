(function () {
    'use strict';
    angular
        .module('desk.ctrl', ['calendar.service'])
        .controller('DeskCtrl', ['CalendarService', DeskCtrl]);

    function DeskCtrl(calendars) {
        var self = this;
        self.calendars = calendars;
    }
})();
