/**
 * @module test.event.ctrl
 * @name EventCtrl
 * @description
 * Tests for EventCtrl under event.ctrl
 * _Enter the test description._
 * */


describe('Controller: event.ctrl.CalendarCtrl', function () {

    // load the controller's module
    beforeEach(module('calendar.ctrl'));

    var ctrl,
        scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        ctrl = $controller('CalendarCtrl', {
            $scope: scope,
            $state: { params: { calendar: "all"}}
        });
    }));

    it('should be defined', function () {
        expect(ctrl).toBeDefined();
    });
});
