describe('Service: calendar.service.CalendarService', function () {

    // load the service's module
    beforeEach(module('calendar.service'));

    // instantiate service
    var service, httpBackend;

    //update the injection
    beforeEach(inject(function (CalendarService, $httpBackend) {
        service = CalendarService;
        httpBackend = $httpBackend;
    }));

    it('should be injected and defined', function () {
        expect(service).toBeDefined();
    });

    it("should provide at least one calendar with keyword 'all'", function () {
        httpBackend.whenGET("data/calendars.json").respond({
            "calendars": {
                "sport": {
                    "sort": 1,
                    "title": "Sport Events"
                },
                "business": {
                    "sort": 2,
                    "title": "Business Events"
                },
                "other": {
                    "sort": 3,
                    "title": "Other Events"
                }
            }
        });
        var calendars = service.list;

        expect(Object.keys(calendars).length).toBe(1);
        httpBackend.flush();
        expect(Object.keys(calendars).length).toBeGreaterThan(1);
        expect(calendars.all).toBeDefined();
        expect(calendars.all.title).toBeDefined();
        expect(calendars.all.sort).toBe(0);
    })


});
