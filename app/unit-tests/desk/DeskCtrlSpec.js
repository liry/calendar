/**
 * @module test.calendar.ctrl
 * @name CalendarCtrl
 * @description
 * Tests for CalendarCtrl under calendar.ctrl
 * _Enter the test description._
 * */


describe('Controller: calendar.ctrl.DeskCtrl', function () {

    // load the controller's module
    beforeEach(module('desk.ctrl'));

    var ctrl,
        scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        ctrl = $controller('DeskCtrl', {
            $scope: scope
        });
    }));

    it('should be defined', function () {
        expect(ctrl).toBeDefined();
    });
});
