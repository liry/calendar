describe('Service: event.service.EventService', function () {

    // load the service's module
    beforeEach(module('event.service'));

    // instantiate service
    var service, httpBackend, rootScope;

    //update the injection
    beforeEach(inject(function (EventService, $httpBackend, $rootScope) {
        scope = $rootScope;
        service = EventService;
        httpBackend = $httpBackend;
        httpBackend.whenGET("data/events.json?calendar=all").respond({
            "events": [
                {
                    "id": 1,
                    "title": "Some Event",
                    "start": "2011-07-14T19:43:37+0100",
                    "end": "2011-07-14T19:43:37+0100",
                    "description": "Some description of some event"
                },
                {
                    "id": 2,
                    "title": "Another Event",
                    "start": "2011-07-14T19:43:37+0100",
                    "end": "2011-07-14T19:43:37+0100",
                    "description": "Some description of another event"
                }
            ]
        });
    }));

    it('should be injected and defined', function () {
        expect(service).toBeDefined();
    });

    it('should send request when call load() and update events', function () {
        service.events = [];
        service.load();
        httpBackend.flush();
        expect(service.list.length).toBe(2);
    });
});
