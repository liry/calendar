describe('Controller: event.ctrl.EventCtrl', function () {

    // load the controller's module
    beforeEach(module('event.ctrl'));

    var ctrl,
        scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        ctrl = $controller('EventCtrl', {
            $scope: scope,
            $state: { params: { event: {}, id: 1}},
            EventService: {}
        });
    }));

    it('should be defined', function () {
        expect(ctrl).toBeDefined();
    });
});
